//= template/modernizr-3.5.0.min.js
//= template/jquery-1.12.4.min.js
//= template/popper.min.js
//= template/bootstrap.min.js
//= template/owl.carousel.min.js
//= template/isotope.pkgd.min.js
//= template/ajax-form.js
//= template/imagesloaded.pkgd.min.js
//= template/scrollIt.js
//= template/jquery.scrollUp.min.js
//= template/wow.min.js
//= template/jquery.slicknav.min.js
//= template/jquery.magnific-popup.min.js
//= template/parallaxie.js
//= template/plugins.js

//= template/contact.js
//= template/jquery.form.js
//= template/jquery.validate.min.js
//= template/mail-script.js
//= template/jquery.ajaxchimp.min.js
//= template/main.js

//= ../node_modules/lazysizes/lazysizes.js
//= ../node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min.js

//= library/slick.js


$(document).ready(function () {
	// slick slider
	$(".blog-slider").each(function (index, item){
		$(item).slick({
			lazyLoad: 'ondemand',
			dots: false,
			infinite: true,
			arrows: false,
			autoplay: true,
			autoplaySpeed: 4000,
			speed: 1000,
			slidesToShow: 3,
			slidesToScroll: 1,
			responsive: [
				{
					breakpoint: 1200,
					settings: {
						slidesToShow: 3,
						slidesToScroll: 1,
						arrows: true,
						centerMode: true,
						focusOnSelect: true,
						centerPadding: "30px"
					}
				},
				{
					breakpoint: 992,
					settings: {
						slidesToShow: 2,
						slidesToScroll: 1,
						arrows: true,
						centerMode: true,
						focusOnSelect: true,
						centerPadding: "30px"
					}
				},
				{
					breakpoint: 767,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1,
						arrows: true,
						centerMode: true,
						focusOnSelect: true,
						centerPadding: "30px"
					}
				}
			]
		});
	});


	$("#modalVideo").on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget); // Button that triggered the modal
		var src = button.attr('data-href'); // Extract info from data-* attributes
		console.log(src);
		var modal = $(this);
		modal.find("iframe").attr('src', src);
	})
	$("#modalVideo").on('hidden.bs.modal', function (e) {
		console.log("close");
		$(this).find("iframe").attr("src", "");
	})
});
